package mtgquarkus.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import mtgquarkus.common.CardSetType;
import mtgquarkus.common.LegalityType;
import mtgquarkus.persistence.model.TbCardLegalities;
import mtgquarkus.persistence.model.TbCardSet;
import mtgquarkus.persistence.model.TbScryfallCard;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class ScryfallJsonUtilities {

    public static TbScryfallCard convertJsonScryfallCard(JsonNode jsonNode, String scryfallId) {
        TbScryfallCard card = new TbScryfallCard();
        card.setScryfallId(scryfallId);
        card.setOracleId(jsonNode.get("oracle_id").asText());
        card.setCardMarketId(jsonNode.get("cardmarket_id") != null ? jsonNode.get("cardmarket_id").asInt() : null);
        card.setScryfallUri(jsonNode.get("scryfall_uri").asText());
        card.setScryfallApiUri(jsonNode.get("uri").asText());
        card.setName(jsonNode.get("name").asText());
        card.setManaCost(jsonNode.get("mana_cost") != null ? jsonNode.get("mana_cost").asText() : null);
        card.setOracleText(jsonNode.get("oracle_text") != null ? jsonNode.get("oracle_text").asText() : null);
        card.setTypeLine(jsonNode.get("type_line").asText());
        card.setRarity(jsonNode.get("rarity").asText());
        card.setSetCode(jsonNode.get("set").asText());
        card.setColors(convertArrayNode(jsonNode.get("colors")));
        card.setColorIdentity(convertArrayNode(jsonNode.get("color_identity")));
        card.setFoil(jsonNode.get("foil").asBoolean());
        card.setReserved(jsonNode.get("reserved").asBoolean());
        card.setBooster(jsonNode.get("booster").asBoolean());
        card.setCollectorNumber(jsonNode.get("collector_number").asText());
        return card;
    }


    public static TbCardLegalities convertJsonCardLegalities(final JsonNode jsonNode, final String scryfallId) {
        TbCardLegalities leg = new TbCardLegalities();
        final JsonNode legalitiesNode = jsonNode.get("legalities");
        leg.setScryfallId(scryfallId);
        leg.setStandard(convertLegalityNode(legalitiesNode.get("standard").asText()));
        leg.setFuture(convertLegalityNode(legalitiesNode.get("future").asText()));
        leg.setHistoric(convertLegalityNode(legalitiesNode.get("historic").asText()));
        leg.setGladiator(convertLegalityNode(legalitiesNode.get("gladiator").asText()));
        leg.setPioneer(convertLegalityNode(legalitiesNode.get("pioneer").asText()));
        leg.setModern(convertLegalityNode(legalitiesNode.get("modern").asText()));
        leg.setLegacy(convertLegalityNode(legalitiesNode.get("legacy").asText()));
        leg.setPauper(convertLegalityNode(legalitiesNode.get("pauper").asText()));
        leg.setVintage(convertLegalityNode(legalitiesNode.get("vintage").asText()));
        leg.setPenny(convertLegalityNode(legalitiesNode.get("penny").asText()));
        leg.setCommander(convertLegalityNode(legalitiesNode.get("commander").asText()));
        leg.setBrawl(convertLegalityNode(legalitiesNode.get("brawl").asText()));
        leg.setDuel(convertLegalityNode(legalitiesNode.get("duel").asText()));
        leg.setOldschool(convertLegalityNode(legalitiesNode.get("oldschool").asText()));
        leg.setPremodern(convertLegalityNode(legalitiesNode.get("premodern").asText()));
        return leg;
    }

    public static TbCardSet convertJsonCardSet(final JsonNode jsonNode) {
        TbCardSet cardSet = new TbCardSet();
        cardSet.setId(jsonNode.get("id").asText());
        cardSet.setCode(jsonNode.get("code").asText());
        cardSet.setMtgoCode(jsonNode.get("mtgo_code") != null ? jsonNode.get("mtgo_code").asText() : null);
        cardSet.setTcgplayerId(jsonNode.get("tcgplayer_id") != null ? jsonNode.get("tcgplayer_id").asInt() : null);
        cardSet.setName(jsonNode.get("name").asText());
        cardSet.setSetType(convertCardSetTypeNode(jsonNode.get("set_type").asText()));
        cardSet.setBlockCode(jsonNode.get("block_code") != null ? jsonNode.get("block_code").asText() : null);
        cardSet.setBlock(jsonNode.get("block") != null ? jsonNode.get("block").asText() : null);
        cardSet.setParentSetCode(jsonNode.get("parent_set_code") != null ? jsonNode.get("parent_set_code").asText() : null);
        cardSet.setCardCount(jsonNode.get("card_count").asInt());
        cardSet.setPrintedSize(jsonNode.get("printed_size") != null ? jsonNode.get("printed_size").asInt() : null);
        cardSet.setDigital(jsonNode.get("digital").asBoolean());
        cardSet.setFoilOnly(jsonNode.get("foil_only").asBoolean());
        cardSet.setNonfoilOnly(jsonNode.get("nonfoil_only").asBoolean());
        cardSet.setScryfallUri(jsonNode.get("scryfall_uri").asText());
        cardSet.setUri(jsonNode.get("uri").asText());
        cardSet.setIconSvgUri(jsonNode.get("icon_svg_uri").asText());
        cardSet.setSearchUri(jsonNode.get("search_uri").asText());
        // FIXME c'e' un modo o datatype migliore?
        final String releasedAt = jsonNode.get("released_at") != null ? jsonNode.get("released_at").asText() : null;
        if (releasedAt != null) {
            final Instant releaseDate = LocalDate.parse(releasedAt).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            cardSet.setReleasedAt(releaseDate);
        }
        return cardSet;
    }

    private static String convertArrayNode(final JsonNode arrayNode) {
        if (arrayNode != null) {
            List<String> nodesAsText = new ArrayList<>();
            for (JsonNode node : arrayNode) {
                nodesAsText.add(node.asText());
            }
            return String.join(";", nodesAsText);
        }
        return null;
    }

    private static LegalityType convertLegalityNode(final String nodeAsText) {
        if (nodeAsText == null || nodeAsText.isEmpty()) {
            return null;
        }
        for (LegalityType t : LegalityType.values()) {
            if (t.getJsonValue().equals(nodeAsText)) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid LegalityType: " + nodeAsText);
    }

    private static CardSetType convertCardSetTypeNode(final String nodeAsText) {
        if (nodeAsText == null || nodeAsText.isEmpty()) {
            return null;
        }
        for (CardSetType t : CardSetType.values()) {
            if (t.getJsonValue().equals(nodeAsText)) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid CardSetType: " + nodeAsText);
    }


}
