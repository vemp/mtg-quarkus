package mtgquarkus.common;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LegalityTypeConverter implements AttributeConverter<LegalityType, String> {

    @Override
    public String convertToDatabaseColumn(LegalityType attribute) {
        return attribute != null ? attribute.getJsonValue() : null;
    }

    @Override
    public LegalityType convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return null;
        }
        for (LegalityType t : LegalityType.values()) {
            if (t.getJsonValue().equals(dbData)) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid LegalityType: " + dbData);
    }
}
