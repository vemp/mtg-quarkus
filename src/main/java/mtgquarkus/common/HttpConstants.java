package mtgquarkus.common;

public class HttpConstants {

    // http codes and messages for swagger
    public static final String HTTP_OK = "200";
    public static final String HTTP_OK_MSG = "OK";
    public static final String HTTP_CREATED = "201";
    public static final String HTTP_CREATED_MSG = "Created";
    public static final String HTTP_NO_CONTENT = "204";
    public static final String HTTP_NO_CONTENT_MSG = "No Content";

    private HttpConstants() {
    }

}
