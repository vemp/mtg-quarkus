package mtgquarkus.common;

import lombok.Getter;

public enum CardSetType {
    CORE("core"),
    EXPANSION("expansion"),
    MASTERS("masters"),
    MASTERPIECE("masterpiece"),
    FROM_THE_VAULT("from_the_vault"),
    SPELLBOOK("spellbook"),
    PREMIUM_DECK("premium_deck"),
    DUEL_DECK("duel_deck"),
    DRAFT_INNOVATION("draft_innovation"),
    TREASURE_CHEST("treasure_chest"),
    COMMANDER("commander"),
    PLANECHASE("planechase"),
    ARCHENEMY("archenemy"),
    VANGUARD("vanguard"),
    FUNNY("funny"),
    STARTER("starter"),
    BOX("box"),
    PROMO("promo"),
    TOKEN("token"),
    MEMORABILIA("memorabilia");

    @Getter
    private String jsonValue;

    CardSetType(String jsonValue) {
        this.jsonValue = jsonValue;
    }


}
