package mtgquarkus.common;

public final class ScryfallResourcesUrl {

    // temporary costant file path, to be deleted later if we retrieve the live data each time
    public static final String JSON_DATA_RESOURCEPATH = "sample-data\\default-cards.json";

    public static final String BULKDATA_URL = "https://api.scryfall.com/bulk-data/default_cards";
    public static final String CARDSETS_URL = "https://api.scryfall.com/sets";

}
