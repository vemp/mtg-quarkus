package mtgquarkus.common;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CardSetTypeConverter implements AttributeConverter<CardSetType, String> {

    @Override
    public String convertToDatabaseColumn(CardSetType attribute) {
        return attribute != null ? attribute.getJsonValue() : null;
    }

    @Override
    public CardSetType convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return null;
        }
        for (CardSetType t : CardSetType.values()) {
            if (t.getJsonValue().equals(dbData)) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid CardSetType: " + dbData);
    }
}
