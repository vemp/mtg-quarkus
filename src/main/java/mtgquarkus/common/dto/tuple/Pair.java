package mtgquarkus.common.dto.tuple;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @see <a href="https://www.javatuples.org/">www.javatuples.org</a>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pair<A, B> {

    private A value0;
    private B value1;

}
