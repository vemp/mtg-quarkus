package mtgquarkus.common;

import lombok.Getter;

public enum LegalityType {
    LEGAL("legal"),
    NOT_LEGAL("not_legal"),
    RESTRICTED("restricted"),
    BANNED("banned");

    @Getter
    private String jsonValue;

    LegalityType(String jsonValue) {
        this.jsonValue = jsonValue;
    }

}
