package mtgquarkus.rest.dto;

import lombok.Data;
import mtgquarkus.common.LegalityType;

@Data
public class Legalities {

    private LegalityType standard;
    private LegalityType future;
    private LegalityType historic;
    private LegalityType gladiator;
    private LegalityType pioneer;
    private LegalityType modern;
    private LegalityType legacy;
    private LegalityType pauper;
    private LegalityType vintage;
    private LegalityType penny;
    private LegalityType commander;
    private LegalityType brawl;
    private LegalityType duel;
    private LegalityType oldschool;
    private LegalityType premodern;

}
