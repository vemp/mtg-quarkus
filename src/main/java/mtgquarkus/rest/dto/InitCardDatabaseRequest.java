package mtgquarkus.rest.dto;

import lombok.Data;

@Data
public class InitCardDatabaseRequest {
    private Boolean refreshFromLiveData;
}
