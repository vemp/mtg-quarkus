package mtgquarkus.rest.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserInventoryCard implements Serializable {
    private String username;
    private String scryfall_id;
    private Integer quantity;
    private Integer quantityFoil;

    private Card card;
}
