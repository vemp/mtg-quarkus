package mtgquarkus.rest.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Card implements Serializable {
    private String scryfallId;
    private String oracleId;
    private Integer cardMarketId;
    private String scryfallUri;
    private String scryfallApiUri;
    private String colorIdentity;
    private String colors;
    private String manaCost;
    private String name;
    private String oracleText;
    private Boolean foil;
    private Boolean reserved;
    private String typeLine;
    private String rarity;
    private String setCode;
    private String collectorNumber;
    private Boolean booster;

    private Legalities legalities;

    private CardSet cardSet;

}
