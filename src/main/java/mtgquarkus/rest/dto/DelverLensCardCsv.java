package mtgquarkus.rest.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class DelverLensCardCsv {

    private String scryfallId;
    private String cardName;
    private String foil;
    private Integer qty;

}
