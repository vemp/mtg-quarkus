package mtgquarkus.rest.dto;

import mtgquarkus.persistence.model.TbCardLegalities;
import mtgquarkus.persistence.model.TbCardSet;
import mtgquarkus.persistence.model.TbScryfallCard;
import mtgquarkus.persistence.model.TbUserInventoryCard;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface DtoMapper {

    Card mapCardEntityToDto(TbScryfallCard card);

    Legalities mapLegalitiesEntityToDto(TbCardLegalities leg);

    CardSet mapCardSetEntityToDto(TbCardSet set);

    UserInventoryCard mapInventoryCardToDto(TbUserInventoryCard inventoryCard);
}
