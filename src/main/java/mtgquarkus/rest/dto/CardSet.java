package mtgquarkus.rest.dto;

import lombok.Data;
import mtgquarkus.common.CardSetType;

import java.io.Serializable;
import java.time.Instant;

/**
 * This class represents a CardSet item.
 * Needs to implement Serializable, otherwise PrimeFaces sorting and filtering won't work
 * See https://stackoverflow.com/questions/5042508/
 */
@Data
public class CardSet implements Serializable {
    private String id;
    private String code;
    private String mtgoCode;
    private Integer tcgplayerId;
    private String name;
    private CardSetType setType;
    private Instant releasedAt;
    private String blockCode;
    private String block;
    private String parentSetCode;
    private Integer cardCount;
    private Integer printedSize;
    private Boolean digital;
    private Boolean foilOnly;
    private Boolean nonfoilOnly;
    private String scryfallUri;
    private String uri;
    private String iconSvgUri;
    private String searchUri;
}
