package mtgquarkus.rest.dto;

import lombok.Data;

@Data
public class AddUserInventoryCardCSVRequest {
    private String username;
    private String filePath;
}
