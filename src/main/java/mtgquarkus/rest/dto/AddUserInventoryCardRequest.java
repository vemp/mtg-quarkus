package mtgquarkus.rest.dto;

import lombok.Data;

@Data
public class AddUserInventoryCardRequest {
    private String username;
    private String scryfall_id;
    private String cardName;
    private String cardSet;
    private Integer quantity;
    private Integer quantityFoil;
}
