package mtgquarkus.rest.resource;

import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.model.TbUser;
import mtgquarkus.persistence.service.UserService;
import mtgquarkus.rest.dto.AddUserRequest;
import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Log4j2
public class UserResource {

    @Inject
    UserService userService;

    @GET
    @Path("/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a single user via his/her username")
    public TbUser getUser(@PathParam("username") String username) {
        log.info("Retrieving user information for username: {}", username);
        return userService.getUser(username);
    }

    @POST
    @Path("/add")
    @Operation(summary = "Add a user with his/her information")
    public void init(AddUserRequest request) {
        log.info("Starting UserResource::init");
        userService.addUser(request.getUsername(), request.getEmail(), request.getPassword());
        log.info("Added new user: {}", request.getUsername());
    }
}
