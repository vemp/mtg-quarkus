package mtgquarkus.rest.resource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.common.ScryfallResourcesUrl;
import mtgquarkus.persistence.model.TbCardLegalities;
import mtgquarkus.persistence.model.TbCardSet;
import mtgquarkus.persistence.model.TbScryfallCard;
import mtgquarkus.persistence.service.CardService;
import mtgquarkus.persistence.service.CardSetService;
import mtgquarkus.rest.dto.Card;
import mtgquarkus.rest.dto.InitCardDatabaseRequest;
import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static mtgquarkus.common.utils.ScryfallJsonUtilities.*;

@RequestScoped
@Path("/card-database")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Log4j2
public class CardDatabaseResource {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Inject
    CardService cardService;

    @Inject
    CardSetService cardSetService;

    @GET
    @Path("/card/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a single card via its scryfall id")
    public Card getCard(@PathParam("id") String id) {
        log.info("Retrieving card information for id {}", id);
        return cardService.getCard(id, false);
    }

    @GET
    @Path("/card/{set}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a single card via its set code and card name")
    public Card getCard(@PathParam("set") String setCode, @PathParam("name") String cardName, @QueryParam("deep") boolean deep) {
        log.info("Retrieving card information for set {} and name {}", setCode, cardName);
        return cardService.getCardByNameAndSet(cardName, setCode, deep);
    }

    @POST
    @Path("/init")
    @Operation(summary = "Initialize card database from Scryfall bulk data")
    public void init(InitCardDatabaseRequest request) {
        log.info("Starting CardDatabaseResource::init");
        try {
            JsonNode defaultCardsNode;
            if (request.getRefreshFromLiveData()) {
                log.info("retrieving default_cards object URL from scryfall REST endpoint...");
                final JsonNode bulkDataNode = OBJECT_MAPPER.readTree(new URL(ScryfallResourcesUrl.BULKDATA_URL));
                final String downloadUri = bulkDataNode.get("download_uri").asText();
                log.info("retrieved download URI for default_cards bulk data: {}", downloadUri);
                defaultCardsNode = OBJECT_MAPPER.readTree(new URL(downloadUri));
            } else {
                File jsonFile = new File(this.getClass().getClassLoader().getResource(ScryfallResourcesUrl.JSON_DATA_RESOURCEPATH).getFile());
                defaultCardsNode = OBJECT_MAPPER.readTree(jsonFile);
            }
            log.info("read {} objects", defaultCardsNode.size());
            List<TbScryfallCard> cardList = new ArrayList<>(defaultCardsNode.size());
            List<TbCardLegalities> legalitiesList = new ArrayList<>(defaultCardsNode.size());
            defaultCardsNode.forEach(o -> {
                // get unique card id
                final String scryfallId = o.get("id").asText();

                // create current card
                cardList.add(convertJsonScryfallCard(o, scryfallId));

                // create current card's legalities info
                legalitiesList.add(convertJsonCardLegalities(o, scryfallId));
            });

            cardService.initCardDatabase(cardList, legalitiesList);

            log.info("wrote {} items on the tb_scryfall_card table", cardList.size());
            log.info("wrote {} items on the tb_card_legalities table", legalitiesList.size());

            final JsonNode cardSetJsonNode = OBJECT_MAPPER.readTree(new URL(ScryfallResourcesUrl.CARDSETS_URL));
            JsonNode jsonArray = cardSetJsonNode.get("data");
            log.info("read {} cardSet objects", jsonArray.size());
            List<TbCardSet> cardSetList = new ArrayList<>(jsonArray.size());
            jsonArray.forEach(o -> {
                // convert current JSON object to card set and add it to our list
                cardSetList.add(convertJsonCardSet(o));
            });

            cardSetService.initCardSetDatabase(cardSetList);
            log.info("wrote {} items on the tb_card_set table", cardSetList.size());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
