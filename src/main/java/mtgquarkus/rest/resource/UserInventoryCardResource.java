package mtgquarkus.rest.resource;

import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.model.pk.UserInventoryCardId;
import mtgquarkus.persistence.service.UserInventoryCardService;
import mtgquarkus.rest.dto.AddUserInventoryCardCSVRequest;
import mtgquarkus.rest.dto.AddUserInventoryCardRequest;
import mtgquarkus.rest.dto.UserInventoryCard;
import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RequestScoped
@Path("/inventory")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Log4j2
public class UserInventoryCardResource {

    @Inject
    UserInventoryCardService userInventoryCardService;

    @GET
    @Path("/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get the inventory of a single user via his/her username")
    public List<UserInventoryCard> getUserInventory(@PathParam("username") String username) {
        log.info("Retrieving user inventory information for username: {}", username);
        return userInventoryCardService.getUserInventory(username);
    }

    @POST
    @Path("/add")
    @Operation(summary = "Add a user inventory item")
    public void addItem(AddUserInventoryCardRequest request) {
        log.info("Starting UserInventoryResource::addItem");
        UserInventoryCardId id = new UserInventoryCardId(request.getUsername(), request.getScryfall_id());
        userInventoryCardService.addItem(id, request.getQuantity(), request.getQuantityFoil());
        log.info("Added new user: {}", request.getUsername());
    }

    @POST
    @Path("/addbyname")
    @Operation(summary = "Add a user inventory item by card name and set")
    public void addItemByCardNameAndSet(AddUserInventoryCardRequest request) {
        log.info("Starting UserInventoryResource::addItemByCardNameAndSet");
        userInventoryCardService.addItemByNameAndSet(request.getUsername(), request.getCardName(), request.getCardSet(), request.getQuantity(), request.getQuantityFoil());
        log.info("Added new user: {}", request.getUsername());
    }

    @POST
    @Path("/importcsv")
    @Operation(summary = "Add cards to a user inventory via a CSV file")
    public void importCsvInventoryForUser(AddUserInventoryCardCSVRequest request) {
        log.info("Starting UserInventoryResource::importCsvInventoryForUser");
//        try {
//            final String filePath = request.getFilePath();
//            final String username = request.getUsername();
//
//        }

    }

}
