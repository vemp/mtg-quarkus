package mtgquarkus.cdi;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.model.TbUser;
import mtgquarkus.persistence.service.UserService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * use only as example, currently detached from all application logic.
 */
@Log4j2
@Named
@RequestScoped
public class HelloMB implements Serializable {

    @Inject
    UserService userService;

    @Getter
    @Setter
    private TbUser myUser;

    @PostConstruct
    void init() {
        final String username = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        myUser = userService.getUser(username);
        log.info(myUser);
    }

    public String getGreetingMessage() {
        return "Hello from Quarkus JSF Application!";
    }

}
