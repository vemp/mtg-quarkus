package mtgquarkus.cdi;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.service.CardService;
import mtgquarkus.persistence.service.UserInventoryCardService;
import mtgquarkus.rest.dto.Card;
import org.primefaces.model.FilterMeta;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Log4j2
@Named  // permette di referenziarlo da una pagina EL
@ViewScoped
public class SetViewerMB {

    @Inject
    CardService cardService;

    @Inject
    UserInventoryCardService inventoryCardService;

    // qui definisco le variabili che mappano input e output per la vista collegata
    @Getter
    private List<Card> cards;

    @Getter
    @Setter
    private List<Card> filteredCards;

    @Getter
    @Setter
    private List<FilterMeta> filterBy;

    @PostConstruct
    void init() {
        if (cards == null) {
            final Map<String, String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            final String set = requestParameterMap.get("s");
            cards = cardService.getCardBySet(set);
            log.info("Retrieved {} cards from set {}", cards.size(), set);
        }
        filterBy = new ArrayList<>();
    }

    public void addOne(String cardId, boolean foil) {
        String username = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        Integer qty = foil ? 0 : 1;
        Integer qtyFoil = foil ? 1 : 0;
        inventoryCardService.addItem(username, cardId, qty, qtyFoil);
        addMessage("Added 1x " + cardId + (foil ? "(foil)" : "") + " to user inventory");
    }

    public void addMessage(String messageString) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, messageString, null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
