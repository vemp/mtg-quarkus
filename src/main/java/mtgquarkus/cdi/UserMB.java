package mtgquarkus.cdi;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.model.TbUser;
import mtgquarkus.persistence.service.UserService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

@Named
@SessionScoped
@Log4j2
public class UserMB implements Serializable {

    @Inject
    UserService userService;

    @Getter
    private String username;

    @PostConstruct
    public void init() {
        log.info("UserMB::init start");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        final HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        if (session != null) {
            String sessionUser = (String) session.getAttribute("user");
            // potrei aver messo in sessione l'intera riga della tabella user per non doverlo rifare qua
            TbUser user = userService.getUser(sessionUser);
            username = user.getUsername();
            return;
        }
        throw new RuntimeException("Session is null");
    }

}