package mtgquarkus.cdi;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.service.UserInventoryCardService;
import mtgquarkus.rest.dto.UserInventoryCard;
import org.primefaces.model.FilterMeta;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Named
@ViewScoped
public class InventoryMB {

    @Inject
    UserInventoryCardService userInventoryCardService;

    @Getter
    private List<UserInventoryCard> inventoryCards;

    @Getter
    @Setter
    private List<UserInventoryCard> filteredInventoryCards;

    @Getter
    @Setter
    private List<FilterMeta> filterBy;

    @PostConstruct
    void init() {
        if (inventoryCards == null) {
            final String username = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
            inventoryCards = userInventoryCardService.getUserInventory(username);
            log.info("Retrieved {} inventory items for user {}", inventoryCards.size(), username);
        }
        filterBy = new ArrayList<>();
    }
}
