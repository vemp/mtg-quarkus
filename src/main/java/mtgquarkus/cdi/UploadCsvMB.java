package mtgquarkus.cdi;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.service.CardService;
import mtgquarkus.persistence.service.UserInventoryCardService;
import mtgquarkus.rest.dto.DelverLensCardCsv;
import org.primefaces.model.file.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Log4j2
@Named
@ViewScoped
public class UploadCsvMB {

    public static final String FOIL = "Foil";

    @Inject
    CardService cardService;

    @Inject
    UserInventoryCardService userInventoryCardService;

    @Getter
    @Setter
    private UploadedFile file;

    public void upload() {
        if (file != null) {
            FacesMessage message = new FacesMessage("Successful", file.getFileName() + "has been uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            try {
                final String csv = new String(file.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
                final CsvMapper mapper = new CsvMapper();
                CsvSchema schema = CsvSchema.builder()
                        .addColumn("scryfallId")
                        .addColumn("cardName")
                        .addColumn("foil")
                        .addColumn("qty")
                        .build();

                final MappingIterator<DelverLensCardCsv> it = mapper.readerFor(DelverLensCardCsv.class).with(schema).readValues(csv);
                final String username = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
                while (it.hasNextValue()) {
                    final DelverLensCardCsv delverLensCardCsv = it.nextValue();
                    final boolean foil = FOIL.equalsIgnoreCase(delverLensCardCsv.getFoil());
                    userInventoryCardService.addItem(username, delverLensCardCsv.getScryfallId(), foil ? 0 : delverLensCardCsv.getQty(), foil ? delverLensCardCsv.getQty() : 0);
                    log.info("Added card(s) with id: {} to user: {}", delverLensCardCsv.getScryfallId(), username);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("Uploaded file: {}", file.getFileName());
        }
    }
}
