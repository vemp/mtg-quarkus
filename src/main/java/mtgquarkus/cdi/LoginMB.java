package mtgquarkus.cdi;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.model.TbUser;
import mtgquarkus.persistence.service.UserService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Log4j2
@Named
@RequestScoped
public class LoginMB implements Serializable {

    public static final String POST_LOGIN_URL = "/app/index.xhtml?faces-redirect=true";

    @Inject
    UserService userService;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @PostConstruct
    void init() {
        log.info("init LoginMB");
    }

    /**
     * la firma del metodo, senza parametri e con return String, me lo identifica come action
     * questo implica che fara' cose e concludera' con un full page reload verso la url ritornata
     *
     * @return
     */
    public String doLogin() {
        log.info("called LoginMB::doLogin with username: {} and password: {}", username, password);
        final TbUser userFromDb = userService.getUser(username);
        if (userFromDb != null && password.equals(userFromDb.getPassword())) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", username);
            return POST_LOGIN_URL;
        }
        return null;
    }


}
