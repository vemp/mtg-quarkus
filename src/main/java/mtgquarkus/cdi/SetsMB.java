package mtgquarkus.cdi;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import mtgquarkus.persistence.service.CardSetService;
import mtgquarkus.rest.dto.CardSet;
import org.primefaces.model.FilterMeta;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Named
@ViewScoped
public class SetsMB implements Serializable {

    @Inject
    CardSetService cardSetService;

    @Getter
    private List<CardSet> sets;

    @Getter
    @Setter
    private List<CardSet> filteredSets;

    @Getter
    @Setter
    private List<FilterMeta> filterBy;

    @PostConstruct
    void init() {
        if (sets == null) {
            sets = cardSetService.getAllNonDigitalSets();
            log.info("Retrieved {} card sets", sets.size());
        }

        filterBy = new ArrayList<>();
    }


}
