package mtgquarkus.login;

import lombok.extern.log4j.Log4j2;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * sei tu Dino, il filter?
 * si sono io, mio padre e'mmorto
 *
 * @see <a href="https://stackoverflow.com/questions/8480100/how-implement-a-login-filter-in-jsf">spiegone sui filtri</a>
 */
@WebFilter("/app/*")
@Log4j2
public class LoginFilter extends HttpFilter {

    private static final String LOGIN_PATH = "/login.xhtml";
    private static final String RESOURCE_IDENTIFIER = "/javax.faces.resource";
    private static final String AJAX_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        log.trace("LoginFilter::doFilter start");
        // recuperiamo la session...
        HttpSession session = request.getSession(false);

        // il contextpath serve per ricostruire l'intero path corretto della pagina di login
        // ed e' utile anche in caso di multi applicazioni sul solito server
        String loginURL = request.getContextPath() + LOGIN_PATH;

        // qui la session e' quella sul server, ma e' sicuramente SOLO quella creata a fronte di una prima connessione
        // di un client, e il cui id e' in un session cookie sul client. Se esiste un attributo user in quella sessione
        // e' SICURAMENTE quello corretto.
        boolean loggedIn = (session != null) && (session.getAttribute("user") != null);

        // la richiesta http mi arriva effettivamente dalla pagina di login?
        boolean loginRequest = request.getRequestURI().equals(loginURL);

        // la richiesta corrente e' per una risorsa (css, js) che non deve essere protetta da security?
        boolean resourceRequest = request.getRequestURI().startsWith(request.getContextPath() + RESOURCE_IDENTIFIER + "/");

        boolean ajaxRequest = "partial/ajax".equals(request.getHeader("Faces-Request"));

        // la richiesta e' per un eventuale contenuto statico non protetto?
//        boolean staticContentRequest = request.getRequestURI().startsWith(request.getContextPath() + CommonPaths.STATIC_RESOURCES_PATH + "/");

        if (loggedIn || loginRequest || resourceRequest) {
            // punte al cazzo sulla cache e il tasto indietro
            // vedi la pagina di SO linkata in javadoc
            if (!resourceRequest) { // Prevent browser from caching restricted resources. See also https://stackoverflow.com/q/4194207/157882
                response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
                response.setDateHeader("Expires", 0); // Proxies.
            }
            log.debug("LoginFilter::doFilter - OK - proceeding to requested page");
            chain.doFilter(request, response);
        } else if (ajaxRequest) {
            log.debug("LoginFilter::doFilter - KO - AJAX request sending redirect");
            response.setContentType("text/xml");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().printf(AJAX_REDIRECT_XML, loginURL); // So, return special XML response instructing JSF ajax to send a redirect.
        } else {
            log.debug("LoginFilter::doFilter - KO - non-AJAX request sending redirect to login");
            response.sendRedirect(loginURL); // So, just perform standard synchronous redirect.
        }
        log.trace("LoginFilter::doFilter end");
    }

}