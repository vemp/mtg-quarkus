package mtgquarkus.persistence.service;

import mtgquarkus.persistence.dao.UserDao;
import mtgquarkus.persistence.model.TbUser;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class UserService {

    @Inject
    UserDao userDao;

    public TbUser getUser(String username) {
        return userDao.get(username);
    }

    public void addUser(String username, String email, String password) {
        TbUser tbUser = new TbUser();
        tbUser.setUsername(username);
        tbUser.setEmail(email);
        tbUser.setPassword(password);
        userDao.persist(tbUser);
        userDao.flush();
    }
}
