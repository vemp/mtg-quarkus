package mtgquarkus.persistence.service;

import mtgquarkus.persistence.dao.UserInventoryCardDao;
import mtgquarkus.persistence.model.TbUserInventoryCard;
import mtgquarkus.persistence.model.pk.UserInventoryCardId;
import mtgquarkus.rest.dto.Card;
import mtgquarkus.rest.dto.DtoMapper;
import mtgquarkus.rest.dto.UserInventoryCard;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class UserInventoryCardService {

    @Inject
    UserInventoryCardDao userInventoryCardDao;

    @Inject
    CardService cardService;

    @Inject
    DtoMapper dtoMapper;

    public List<UserInventoryCard> getUserInventory(String username) {
        final List<TbUserInventoryCard> tbUserInventoryCards = userInventoryCardDao.findByUsername(username);
        if (!tbUserInventoryCards.isEmpty()) {
            final List<String> idList = tbUserInventoryCards.stream().map(TbUserInventoryCard::getScryfall_id).collect(Collectors.toList());
            final Map<String, Card> cards = cardService.getCards(idList).stream().collect(Collectors.toMap(Card::getScryfallId, Function.identity()));
            final List<UserInventoryCard> inventoryCards = tbUserInventoryCards.stream().map(e -> dtoMapper.mapInventoryCardToDto(e)).collect(Collectors.toList());
            inventoryCards.forEach(c -> c.setCard(cards.get(c.getScryfall_id())));
            return inventoryCards;
        }
        return Collections.emptyList();
    }

    public void addItem(UserInventoryCardId key, Integer qty, Integer qtyFoil) {
        final TbUserInventoryCard item = userInventoryCardDao.get(key);
        if (item == null) {
            TbUserInventoryCard newItem = new TbUserInventoryCard();
            newItem.setUsername(key.getUsername());
            newItem.setScryfall_id(key.getScryfall_id());
            newItem.setQuantity(qty);
            newItem.setQuantityFoil(qtyFoil);
            userInventoryCardDao.persist(newItem);
        } else {
            item.setQuantity(item.getQuantity() + qty);
            item.setQuantityFoil(item.getQuantityFoil() + qtyFoil);
            userInventoryCardDao.persist(item);
        }
    }

    public void addItem(String username, String scryfallId, Integer qty, Integer qtyFoil) {
        UserInventoryCardId id = new UserInventoryCardId(username, scryfallId);
        addItem(id, qty, qtyFoil);
    }

    public void addItemByNameAndSet(String username, String cardName, String cardSet, Integer qty, Integer qtyFoil) {
        final Card card = cardService.getCardByNameAndSet(cardName, cardSet, false);
        if (card != null) {
            addItem(new UserInventoryCardId(username, card.getScryfallId()), qty, qtyFoil);
        }
    }

}
