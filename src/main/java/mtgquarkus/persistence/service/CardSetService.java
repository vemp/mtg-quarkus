package mtgquarkus.persistence.service;

import mtgquarkus.persistence.dao.CardSetDao;
import mtgquarkus.persistence.model.TbCardSet;
import mtgquarkus.rest.dto.CardSet;
import mtgquarkus.rest.dto.DtoMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class CardSetService {

    @Inject
    CardSetDao cardSetDao;

    @Inject
    DtoMapper dtoMapper;

    public void initCardSetDatabase(List<TbCardSet> cardSetsList) {
        cardSetDao.merge(cardSetsList);
        cardSetDao.flush();
    }

    public CardSet getCardSet(String code) {
        final TbCardSet cardSet = cardSetDao.findByCode(code);
        return dtoMapper.mapCardSetEntityToDto(cardSet);
    }

    public List<CardSet> getAllSets() {
        final List<TbCardSet> cardSetList = cardSetDao.findAll();
        return cardSetList.stream().map(c -> dtoMapper.mapCardSetEntityToDto(c)).collect(Collectors.toList());
    }

    public List<CardSet> getAllNonDigitalSets() {
        final List<TbCardSet> cardSetList = cardSetDao.findAllNonDigital();
        return cardSetList.stream().map(c -> dtoMapper.mapCardSetEntityToDto(c)).collect(Collectors.toList());
    }

}
