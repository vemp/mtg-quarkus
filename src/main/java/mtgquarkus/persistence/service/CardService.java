package mtgquarkus.persistence.service;

import mtgquarkus.common.dto.tuple.Pair;
import mtgquarkus.persistence.dao.CardLegalitiesDao;
import mtgquarkus.persistence.dao.ScryfallCardDao;
import mtgquarkus.persistence.model.TbCardLegalities;
import mtgquarkus.persistence.model.TbCardSet;
import mtgquarkus.persistence.model.TbScryfallCard;
import mtgquarkus.rest.dto.Card;
import mtgquarkus.rest.dto.DtoMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class CardService {

    @Inject
    ScryfallCardDao scryfallCardDao;

    @Inject
    CardLegalitiesDao cardLegalitiesDao;

    @Inject
    DtoMapper dtoMapper;


    public void initCardDatabase(List<TbScryfallCard> cardList, List<TbCardLegalities> legalitiesList) {
        // select all -> all ATTACHED entities
        final List<TbScryfallCard> all = scryfallCardDao.findAll();
        final List<TbCardLegalities> all2 = cardLegalitiesDao.findAll();
        // now we hope that since all entities from the db have been read via the above SELECT,
        // it will do merge using the batch size and not 1 by 1
        scryfallCardDao.merge(cardList);
        cardLegalitiesDao.merge(legalitiesList);
        scryfallCardDao.flush();
    }

    public Card getCard(String cardId, boolean deep) {
        final TbScryfallCard tbScryfallCard = scryfallCardDao.get(cardId);
        final TbCardLegalities tbCardLegalities = cardLegalitiesDao.get(cardId);
        Card card = dtoMapper.mapCardEntityToDto(tbScryfallCard);
        card.setLegalities(dtoMapper.mapLegalitiesEntityToDto(tbCardLegalities));
        return card;
    }

    public List<Card> getCards(List<String> cardIdList) {
        final List<TbScryfallCard> tbScryfallCards = scryfallCardDao.find(cardIdList);
        final Map<String, TbCardLegalities> tbCardLegalities = cardLegalitiesDao.find(cardIdList);
        final List<Card> cards = tbScryfallCards.stream().map(e -> dtoMapper.mapCardEntityToDto(e)).collect(Collectors.toList());
        cards.forEach(c -> c.setLegalities(dtoMapper.mapLegalitiesEntityToDto(tbCardLegalities.get(c.getScryfallId()))));
        return cards;
    }

    public List<Card> getCardBySet(String set) {
        final List<TbScryfallCard> scryfallCardList = scryfallCardDao.findBySet(set);
        if (!scryfallCardList.isEmpty()) {
            return scryfallCardList.stream()
                    .map(c -> dtoMapper.mapCardEntityToDto(c))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public Card getCardByNameAndSet(String cardName, String set, boolean deep) {
        if (deep) {
            final Pair<TbScryfallCard, TbCardSet> pair = scryfallCardDao.findByNameAndSetDeep(cardName, set);
            if (pair != null) {
                final TbCardLegalities cardLegalities = cardLegalitiesDao.get(pair.getValue0().getScryfallId());
                final Card card = dtoMapper.mapCardEntityToDto(pair.getValue0());
                card.setLegalities(dtoMapper.mapLegalitiesEntityToDto(cardLegalities));
                card.setCardSet(dtoMapper.mapCardSetEntityToDto(pair.getValue1()));
                return card;
            }
        } else {
            final TbScryfallCard scryfallCard = scryfallCardDao.findByNameAndSet(cardName, set);
            if (scryfallCard != null) {
                final TbCardLegalities cardLegalities = cardLegalitiesDao.get(scryfallCard.getScryfallId());
                Card card = dtoMapper.mapCardEntityToDto(scryfallCard);
                card.setLegalities(dtoMapper.mapLegalitiesEntityToDto(cardLegalities));
                return card;
            }
        }
        return null;
    }

}
