package mtgquarkus.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tb_scryfall_card")
@Getter
@Setter
@ToString
public class TbScryfallCard implements Serializable {

    @Id
    @Column(name = "scryfall_id")
    private String scryfallId;

    @Column(name = "oracle_id")
    private String oracleId;

    @Column(name = "cardmarket_id")
    private Integer cardMarketId;

    @Column(name = "scryfall_uri")
    private String scryfallUri;

    @Column(name = "scryfall_api_uri")
    private String scryfallApiUri;

    @Column(name = "color_identity")
    private String colorIdentity;

    @Column(name = "colors")
    private String colors;

    @Column(name = "foil")
    private boolean foil;

    @Column(name = "mana_cost")
    private String manaCost;

    @Column(name = "name")
    private String name;

    @Column(name = "oracle_text")
    private String oracleText;

    @Column(name = "reserved")
    private Boolean reserved;

    @Column(name = "type_line")
    private String typeLine;

    @Column(name = "rarity")
    private String rarity;

    @Column(name = "set_code")
    private String setCode;

    @Column(name = "collector_number")
    private String collectorNumber;

    @Column(name = "booster")
    private Boolean booster;


}
