package mtgquarkus.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import mtgquarkus.common.CardSetType;
import mtgquarkus.common.CardSetTypeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "tb_card_set")
@Getter
@Setter
@ToString
public class TbCardSet implements Serializable {


    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "code")
    private String code;

    @Column(name = "mtgo_code")
    private String mtgoCode;

    @Column(name = "tcgplayer_id")
    private Integer tcgplayerId;

    @Column(name = "name")
    private String name;

    @Column(name = "set_type")
    @Convert(converter = CardSetTypeConverter.class)
    private CardSetType setType;

    @Column(name = "released_at")
    private Instant releasedAt;

    @Column(name = "block_code")
    private String blockCode;

    @Column(name = "block")
    private String block;

    @Column(name = "parent_set_code")
    private String parentSetCode;

    @Column(name = "card_count")
    private Integer cardCount;

    @Column(name = "printed_size")
    private Integer printedSize;

    @Column(name = "digital")
    private boolean digital;

    @Column(name = "foil_only")
    private boolean foilOnly;

    @Column(name = "nonfoil_only")
    private boolean nonfoilOnly;

    @Column(name = "scryfall_uri")
    private String scryfallUri;

    @Column(name = "uri")
    private String uri;

    @Column(name = "icon_svg_uri")
    private String iconSvgUri;

    @Column(name = "search_uri")
    private String searchUri;

}
