package mtgquarkus.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import mtgquarkus.persistence.model.pk.UserInventoryCardId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(UserInventoryCardId.class)
@Table(name = "tb_user_inventory_card")
@Getter
@Setter
@ToString
public class TbUserInventoryCard implements Serializable {

    @Id
    private String username;

    @Id
    private String scryfall_id;

    @Column(name = "qty")
    private Integer quantity;

    @Column(name = "qty_foil")
    private Integer quantityFoil;
}
