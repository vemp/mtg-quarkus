package mtgquarkus.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import mtgquarkus.common.LegalityType;
import mtgquarkus.common.LegalityTypeConverter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_card_legalities")
@Getter
@Setter
@ToString
public class TbCardLegalities implements Serializable {

    @Id
    @Column(name = "scryfall_id")
    private String scryfallId;


    @Column(name = "standard")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType standard;

    @Column(name = "future")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType future;

    @Column(name = "historic")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType historic;

    @Column(name = "gladiator")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType gladiator;

    @Column(name = "pioneer")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType pioneer;

    @Column(name = "modern")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType modern;

    @Column(name = "legacy")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType legacy;

    @Column(name = "pauper")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType pauper;

    @Column(name = "vintage")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType vintage;

    @Column(name = "penny")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType penny;

    @Column(name = "commander")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType commander;

    @Column(name = "brawl")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType brawl;

    @Column(name = "duel")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType duel;

    @Column(name = "oldschool")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType oldschool;

    @Column(name = "premodern")
    @Convert(converter = LegalityTypeConverter.class)
    private LegalityType premodern;
}
