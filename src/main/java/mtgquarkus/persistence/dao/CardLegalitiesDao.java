package mtgquarkus.persistence.dao;

import mtgquarkus.persistence.model.TbCardLegalities;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestScoped
@Transactional(Transactional.TxType.MANDATORY)
public class CardLegalitiesDao extends BaseSimplePkEntityDao<TbCardLegalities, String> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CardLegalitiesDao() {
        super(TbCardLegalities.class, "scryfall_id");
    }

    public Map<String, TbCardLegalities> find(List<String> idList) {
        TypedQuery<TbCardLegalities> query = getEntityManager().createQuery("SELECT t FROM TbCardLegalities t WHERE t.id IN :idlist", TbCardLegalities.class);
        query.setParameter("idlist", idList);
        return query.getResultList().stream()
                .collect(Collectors.toMap(TbCardLegalities::getScryfallId, Function.identity()));
    }

    public List<TbCardLegalities> findAll() {
        TypedQuery<TbCardLegalities> query = getEntityManager().createQuery("SELECT t from TbCardLegalities t", TbCardLegalities.class);
        return query.getResultList();
    }

}
