package mtgquarkus.persistence.dao;


import mtgquarkus.common.dto.tuple.Pair;
import mtgquarkus.persistence.model.TbCardSet;
import mtgquarkus.persistence.model.TbScryfallCard;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@RequestScoped
@Transactional(Transactional.TxType.MANDATORY)
public class ScryfallCardDao extends BaseSimplePkEntityDao<TbScryfallCard, String> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScryfallCardDao() {
        super(TbScryfallCard.class, "scryfall_id");
    }

    public List<TbScryfallCard> find(List<String> idList) {
        TypedQuery<TbScryfallCard> query = getEntityManager().createQuery("SELECT t FROM TbScryfallCard t WHERE t.id IN :idlist", TbScryfallCard.class);
        query.setParameter("idlist", idList);
        return query.getResultList();
    }

    public List<TbScryfallCard> findAll() {
        TypedQuery<TbScryfallCard> query = getEntityManager().createQuery("SELECT t FROM TbScryfallCard t", TbScryfallCard.class);
        return query.getResultList();
    }

    public List<TbScryfallCard> findBySet(String set) {
        TypedQuery<TbScryfallCard> query = getEntityManager().createQuery("SELECT t from TbScryfallCard t WHERE t.setCode=:set ORDER BY t.collectorNumber", TbScryfallCard.class);
        query.setParameter("set", set);
        return query.getResultList();
    }

    public TbScryfallCard findByNameAndSet(String cardName, String set) {
        try {
            TypedQuery<TbScryfallCard> query = getEntityManager().createQuery("SELECT c FROM TbScryfallCard c WHERE c.name=:name AND c.setCode=:set", TbScryfallCard.class);
            query.setParameter("name", cardName);
            query.setParameter("set", set);
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public Pair<TbScryfallCard, TbCardSet> findByNameAndSetDeep(String cardName, String set) {
        try {
            TypedQuery<Object[]> query = getEntityManager().createQuery("SELECT c, s FROM TbScryfallCard c, TbCardSet s WHERE c.setCode=s.code AND c.name=:name AND c.setCode=:set", Object[].class);
            query.setParameter("name", cardName);
            query.setParameter("set", set);
            final Object[] result = query.getSingleResult();
            return new Pair<>((TbScryfallCard) result[0], (TbCardSet) result[1]);
        } catch (NoResultException ex) {
            return null;
        }
    }

}
