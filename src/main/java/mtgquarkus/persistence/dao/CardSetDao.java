package mtgquarkus.persistence.dao;

import mtgquarkus.persistence.model.TbCardSet;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@RequestScoped
@Transactional(Transactional.TxType.MANDATORY)
public class CardSetDao extends BaseSimplePkEntityDao<TbCardSet, String> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CardSetDao() {
        super(TbCardSet.class, "id");
    }

    public List<TbCardSet> findAll() {
        TypedQuery<TbCardSet> query = getEntityManager().createQuery("SELECT t FROM TbCardSet t", TbCardSet.class);
        return query.getResultList();
    }

    public TbCardSet findByCode(String code) {
        TypedQuery<TbCardSet> query = getEntityManager().createQuery("SELECT t FROM TbCardSet t WHERE t.code=:code", TbCardSet.class);
        query.setParameter("code", code);
        return query.getSingleResult();
    }

    public List<TbCardSet> findAllNonDigital() {
        TypedQuery<TbCardSet> query = getEntityManager().createQuery("SELECT t FROM TbCardSet t WHERE t.digital=:digital", TbCardSet.class);
        query.setParameter("digital", false);
        return query.getResultList();
    }

}
