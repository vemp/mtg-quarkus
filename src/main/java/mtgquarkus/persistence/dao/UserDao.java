package mtgquarkus.persistence.dao;

import mtgquarkus.persistence.model.TbUser;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@RequestScoped
@Transactional(Transactional.TxType.MANDATORY)
public class UserDao extends BaseSimplePkEntityDao<TbUser, String> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserDao() {
        super(TbUser.class, "username");
    }

    public TbUser findByEmail(String email) {
        try {
            TypedQuery<TbUser> query = em.createQuery("SELECT t FROM TbUser WHERE t.email=:email", TbUser.class);
            query.setParameter("email", email);
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

}
