package mtgquarkus.persistence.dao;

import javax.persistence.Query;
import java.io.Serializable;

public abstract class BaseSimplePkEntityDao<E extends Serializable, K> extends BaseEntityDao<E, K> {

    private final String keyColumnName;

    public BaseSimplePkEntityDao(Class<E> entityClass, String keyColumnName) {
        super(entityClass);
        this.keyColumnName = keyColumnName;
    }

    public int delete(K key) {
        String qs = "DELETE FROM " + entityClass.getSimpleName() + " t WHERE t." + keyColumnName + " = :key";
        Query q = getEntityManager().createQuery(qs);
        q.setParameter("key", key);
        int cnt = q.executeUpdate();
        return cnt;
    }

}
