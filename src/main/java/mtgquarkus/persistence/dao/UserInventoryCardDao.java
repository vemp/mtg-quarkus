package mtgquarkus.persistence.dao;

import mtgquarkus.persistence.model.TbUserInventoryCard;
import mtgquarkus.persistence.model.pk.UserInventoryCardId;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@RequestScoped
@Transactional(Transactional.TxType.MANDATORY)
public class UserInventoryCardDao extends BaseEntityDao<TbUserInventoryCard, UserInventoryCardId> {

    @PersistenceContext
    EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public UserInventoryCardDao() {
        super(TbUserInventoryCard.class);
    }

    public int delete(UserInventoryCardId key) {
        String qs = "DELETE FROM " + entityClass.getSimpleName() + " t WHERE t.username = :username AND t.scryfall_id = :scryfall_id";
        Query q = getEntityManager().createQuery(qs);
        q.setParameter("username", key.getUsername());
        q.setParameter("scryfall_id", key.getScryfall_id());
        int cnt = q.executeUpdate();
        return cnt;
    }

    public List<TbUserInventoryCard> findByUsername(String username) {
        TypedQuery<TbUserInventoryCard> typedQuery = getEntityManager()
                .createQuery("SELECT t FROM " + entityClass.getSimpleName() + " t" +
                        " WHERE t.username=:username" +
                        " ORDER BY t.scryfall_id", TbUserInventoryCard.class);
        typedQuery.setParameter("username", username);
        return typedQuery.getResultList();
    }


}
