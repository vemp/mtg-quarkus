package mtgquarkus.persistence.dao;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public abstract class BaseEntityDao<E extends Serializable, K> {

    protected final Class<E> entityClass;

    public BaseEntityDao(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void flush() {
        getEntityManager().flush();
    }

    public E get(K key) {
        return getEntityManager().find(entityClass, key);
    }

    public void persist(E ent) {
        getEntityManager().persist(ent);
    }

    public void persist(List<E> entityList) {
        if (entityList != null) {
            entityList.forEach(this::persist);
        }
    }

    public void merge(E ent) {
        getEntityManager().merge(ent);
    }

    public void merge(List<E> entityList) {
        if (entityList != null) {
            entityList.forEach(this::merge);
        }
    }

}