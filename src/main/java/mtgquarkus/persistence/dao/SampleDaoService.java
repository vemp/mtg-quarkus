package mtgquarkus.persistence.dao;

import mtgquarkus.persistence.model.TbSample;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class SampleDaoService {

    @PersistenceContext
    EntityManager em;

    public TbSample getSampleById(Long sampleId) {
        return em.find(TbSample.class, sampleId);
    }

    public TbSample persist() {
        TbSample ent = new TbSample();
        Date now = new Date();
        ent.setCreateTms(now);
        ent.setUpdateTms(now);
        ent.setCurValue("asd");
        em.persist(ent);
        return ent;
    }

}
