-- reset schema
DO 'DECLARE
    rec RECORD;
BEGIN
    FOR rec IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
        EXECUTE ''DROP TABLE IF EXISTS '' || quote_ident(rec.tablename) || '' CASCADE'';
    END LOOP;
END';

DROP TABLE IF EXISTS tb_user CASCADE;
CREATE TABLE IF NOT EXISTS tb_user (
    username TEXT PRIMARY KEY
    , email TEXT NOT NULL
    , password TEXT NOT NULL
);

DROP TABLE IF EXISTS tb_scryfall_card CASCADE;
CREATE TABLE IF NOT EXISTS tb_scryfall_card (
    scryfall_id TEXT PRIMARY KEY
    , oracle_id TEXT NOT NULL
    , cardmarket_id INTEGER
    , scryfall_uri TEXT NOT NULL
    , scryfall_api_uri TEXT
    , color_identity TEXT
    , colors TEXT
    , foil BOOLEAN
    , mana_cost TEXT
    , name TEXT
    , oracle_text TEXT
    , reserved BOOLEAN
    , type_line TEXT
    , rarity TEXT
    , set_code TEXT
    , collector_number TEXT
    , booster BOOLEAN
);

DROP TABLE IF EXISTS tb_card_legalities CASCADE;
CREATE TABLE IF NOT EXISTS tb_card_legalities (
    scryfall_id TEXT PRIMARY KEY REFERENCES tb_scryfall_card(scryfall_id)
    , standard TEXT NOT NULL
    , future TEXT NOT NULL
    , historic TEXT NOT NULL
    , gladiator TEXT NOT NULL
    , pioneer TEXT NOT NULL
    , modern TEXT NOT NULL
    , legacy TEXT NOT NULL
    , pauper TEXT NOT NULL
    , vintage TEXT NOT NULL
    , penny TEXT NOT NULL
    , commander TEXT NOT NULL
    , brawl TEXT NOT NULL
    , duel TEXT NOT NULL
    , oldschool TEXT NOT NULL
    , premodern TEXT NOT NULL
    , CHECK(standard IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(future IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(historic IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(gladiator IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(pioneer IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(modern IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(legacy IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(pauper IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(vintage IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(penny IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(commander IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(brawl IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(duel IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(oldschool IN ('legal', 'not_legal', 'restricted', 'banned'))
    , CHECK(premodern IN ('legal', 'not_legal', 'restricted', 'banned'))
);

DROP TABLE IF EXISTS tb_user_inventory_card CASCADE;
CREATE TABLE IF NOT EXISTS tb_user_inventory_card (
    username TEXT NOT NULL REFERENCES tb_user(username)
    , scryfall_id TEXT NOT NULL REFERENCES tb_scryfall_card(scryfall_id)
    , qty INTEGER
    , qty_foil INTEGER
    , PRIMARY KEY (username, scryfall_id)
);

DROP TABLE IF EXISTS tb_card_set CASCADE;
CREATE TABLE IF NOT EXISTS tb_card_set (
    id TEXT PRIMARY KEY
    , code TEXT NOT NULL
    , mtgo_code TEXT
    , tcgplayer_id INTEGER
    , name TEXT NOT NULL
    , set_type TEXT NOT NULL
    , released_at TIMESTAMP
    , block_code TEXT
    , block TEXT
    , parent_set_code TEXT
    , card_count INTEGER NOT NULL
    , printed_size INTEGER
    , digital BOOLEAN NOT NULL
    , foil_only BOOLEAN NOT NULL
    , nonfoil_only BOOLEAN NOT NULL
    , scryfall_uri TEXT NOT NULL
    , uri TEXT NOT NULL
    , icon_svg_uri TEXT NOT NULL
    , search_uri TEXT NOT NULL
    , CHECK(set_type IN ('core','expansion','masters','masterpiece','from_the_vault','spellbook','premium_deck','duel_deck','draft_innovation','treasure_chest','commander','planechase','archenemy','vanguard','funny','starter','box','promo','token','memorabilia'))
);
